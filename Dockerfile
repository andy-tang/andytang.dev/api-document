# syntax = docker/dockerfile:experimental
#--------------------------------------------------------------------------------
# Stage 1: Compile React.js
#--------------------------------------------------------------------------------
FROM node:lts-alpine as builder
LABEL stage=intermediate

WORKDIR /opt/app
COPY package*.json ./
RUN --mount=type=cache,target=/opt/app/.npm \
    npm set cache /opt/app/.npm && \
    npm ci

COPY . .
RUN npm run build

#--------------------------------------------------------------------------------
# Stage 2: Prepare running environment for java container
#--------------------------------------------------------------------------------
FROM nginx:alpine

COPY --from=builder /opt/app/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]
