
import { RedocStandalone } from 'redoc';

const specs = {
  default: require('./assets/swagger.json')
}

function App() {
  return (
    <>
      <RedocStandalone
        spec={specs.default}
        options={{
          hideDownloadButton: true,
          hideLoading: true,
          menuToggle: true,
          noAutoAuth: true,
          theme: {
            colors: { primary: { main: '#0063a9' } },
            logo: { gutter: '1.5em' }
          },
        }} />
    </>
  );
}

export default App;
